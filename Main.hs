module Main where

import Control.Applicative
import Control.Concurrent ( threadDelay )
import Control.Distributed.Process
import Control.Distributed.Process.Async
import Control.Distributed.Process.Backend.SimpleLocalnet
import Control.Distributed.Process.Closure
import Control.Distributed.Process.Debug ( enableTrace )
import Control.Distributed.Process.Extras hiding
    ( newChan, sendChan, __remoteTable )
import Control.Distributed.Process.Extras.Time
import Control.Distributed.Process.ManagedProcess hiding
    ( call )
import Control.Distributed.Process.Node ( initRemoteTable )
import Control.Monad
import Data.Binary
import Data.ByteString ( ByteString )
import Data.Foldable ( foldlM )
import Data.Typeable
import GHC.Generics
import System.Environment ( getArgs )

import qualified Control.Distributed.Process.Extras as PE
import qualified Control.Distributed.Process.ManagedProcess as MP
import qualified Control.Distributed.Process.Node as Node
import qualified Data.ByteString.Char8 as BC
import qualified Data.List as L

-- | Product numbers from to
data Prod = Prod Integer Integer
            deriving (Show, Ord, Eq, Typeable, Generic)
instance Binary Prod



-- serverProcess :: Process ()
-- serverProcess = do
--     let p = statelessProcess
--             { apiHandlers =
--                       [ handleCall_ $ \(Prod from to) -> do
--                                 return $ product [from..to ]
--                       ]
--             }
--     serve () (statelessInit Infinity) p

evalProd :: Prod -> Process Integer
evalProd x@(Prod from to) = do
    liftIO $ putStrLn $ show x
    return $ product [from..to]

$(remotable ['evalProd])

remoteTable :: RemoteTable
remoteTable =
    PE.__remoteTable
    $ __remoteTable initRemoteTable

runSlave :: String -> String -> IO ()
runSlave host port = do
    b <- initializeBackend host port remoteTable
    startSlave b


runMaster :: String -> String -> Integer -> Integer -> IO ()
runMaster host port portion amount = do
    b <- initializeBackend host port remoteTable
    startMaster b $ masterProcess portion amount

masterProcess :: Integer -> Integer -> [NodeId] -> Process ()
masterProcess portion amount nodes = do
    let ranges = buildRange portion 1 amount
        robin = zip (cycle nodes) ranges
    handles <- forM robin $ \(node, (from, to)) -> do
        async $ AsyncRemoteTask
              $(functionTDict 'evalProd)
              node
              $ $(mkClosure 'evalProd)
              $ Prod from to
    res <- foldlM waitMult 1 handles

    liftIO $ putStrLn $ show res
  where
    waitMult acc handle = do
        res <- wait handle
        case res of
            AsyncDone a -> return $ acc * a
            e -> die $ show e


buildRange :: Integer -> Integer -> Integer -> [(Integer, Integer)]
buildRange n from to = go from
  where
    go start =
        let lim = min to $ start + n - 1
            h = (start, lim)
            t = if lim == to
                then []
                else go $ lim + 1
        in h:t



main = do
    args <- getArgs
    case args of
        ["slave", host, port] -> runSlave host port
        ["master", host, port, portion, value] ->
            runMaster host port (read portion) (read value)
        _ -> putStrLn "what ?"
